package tmp.seleniumgui.popups;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import tmp.seleniumgui.pages.BasePage;
import tmp.seleniumgui.pages.MailBoxPage;

import java.time.Duration;

public class LoginPopUp extends BasePage {


    private static  String RED= "#ED0A34";

    @FindBy(css = "[name='username']")
    public WebElement usernameInput;
    @FindBy(css = "[name='password']")
    public WebElement passwordInput;
    @FindBy(css = "div[class^='Select__value-container']")
    public WebElement mailDomainDropDown;
    @FindBy(css = "[data-test-id='next-button']")
    public WebElement enterPassBtn;
    @FindBy(css = "[data-test-id='submit-button']")
    public WebElement signInBtn;
    @FindBy(css = "[data-test-id='error-footer-text'] small")
    public WebElement singInErrorMessage;


    public LoginPopUp(WebDriver driver){
       super(driver);
        PageFactory.initElements(driver,this);
    }

    private void switchToLoginFrame(){
        new WebDriverWait(driver, Duration.ofSeconds(15))
                .until(driver -> driver.findElement(By.xpath("//iframe[@class='ag-popup__frame__layout__iframe']")));
        driver.switchTo().
                frame(driver.findElement(By.xpath("//iframe[@class='ag-popup__frame__layout__iframe']")));
    }
    @Step("Enter email={0} and password={1}")
    public MailBoxPage login(String username, String password){
        switchToLoginFrame();
        usernameInput.sendKeys(username);
        enterPassBtn.click();
        passwordInput.sendKeys(password);
        signInBtn.click();
        return new MailBoxPage(driver);
    }

    public LoginPopUp fillEmail(String  email){
        switchToLoginFrame();
        waitVisible(usernameInput, 7);
        usernameInput.sendKeys(email);
        return  this;
    }


    public  LoginPopUp clickEnterPassBtn(){
        enterPassBtn.click();
        return this;
    }
    public String getErrorText(){
        return  singInErrorMessage.getText();
    }





}
