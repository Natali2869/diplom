package tmp.seleniumgui.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class MailBoxPage extends BasePage{
    WebElement whiteLineEmail = driver.findElement(By.cssSelector("[data-testid='whiteline-account']"));

    public MailBoxPage(WebDriver driver) {
        super(driver);
    }

    @Step("Validate that user({0}) is logged in ")
    public void isLoggedIn(String userName){
        waitVisible(whiteLineEmail, 15);
        assertThat(whiteLineEmail.isDisplayed()).as("tmp.selenium.pojo.User email should be visible").isTrue();
        assertThat(whiteLineEmail.getText()).isEqualToIgnoringCase(userName);
    }

}
