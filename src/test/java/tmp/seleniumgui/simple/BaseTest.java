package tmp.seleniumgui.simple;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import tmp.selenium.pojo.User;
import tmp.selenium.util.Config;
import tmp.selenium.util.WebdriverFactory;
import tmp.seleniumgui.util.TestListener;

@ExtendWith(TestListener.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTest {
    public static WebDriver driver;
    Config config = Config.getConfig();
    static User Admin ;
    @BeforeEach
    void setup(){
        Admin = new User(config.getUserName(),config.getPassword());
        if(driver!= null)
            driver.close();
        driver = new WebdriverFactory().create();

    }

    @AfterAll
    void tearDown(){
        driver.quit();
    }



}
