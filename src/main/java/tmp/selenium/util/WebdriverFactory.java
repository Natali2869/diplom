package tmp.selenium.util;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.time.Duration;

public class WebdriverFactory {
    WebDriver driver;
    Config config = Config.getConfig();

    public WebDriver create() {

        if (config.getBrowser().equals("chrome"))
            this.driver = chromeDriver();
        if (config.getBrowser().equals("firefox"))
            return firefoxDriver();

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        return driver;
    }


    private WebDriver chromeDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options;
        switch (config.getDevice()) {
            case "desktop":
                options = chromeDesktop();
                break;
            case "notebook":
                options = chromeNotebook();
                break;
            case "mobile":
                options = chromeMobile();
                break;
            default:
                options = chromeDesktop();
        }
        return new ChromeDriver(options);
    }

    private WebDriver firefoxDriver() {
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options;
        switch (config.getDevice()) {
            case "desktop":
                options = firefoxDesktop();
                break;
            case "notebook":
                options = firefoxNotebook();
                break;
            case "mobile":
                options = firefoxMobile();
                break;
            default:
                options = firefoxDesktop();
        }
        return new FirefoxDriver(options);

    }

    private ChromeOptions chromeDesktop() {
        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        options.addArguments("--window-size=1920,1080");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        return options.merge(capabilities);
    }

    private ChromeOptions chromeNotebook() {
        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        options.addArguments("--window-size=1200,768");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        return options.merge(capabilities);
    }

    private ChromeOptions chromeMobile() {
        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        options.addArguments("--window-size=360,640");
        return options.merge(capabilities);
    }

    private FirefoxOptions firefoxDesktop() {
        FirefoxOptions options = new FirefoxOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        options.addArguments("--width=1920");
        options.addArguments("--height=1080");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        return options.merge(capabilities);
    }

    private FirefoxOptions firefoxNotebook() {
        FirefoxOptions options = new FirefoxOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        options.addArguments("--width=1200");
        options.addArguments("--height=768");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        return options.merge(capabilities);
    }

    private FirefoxOptions firefoxMobile() {
        FirefoxOptions options = new FirefoxOptions();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        options.addArguments("--width=360");
        options.addArguments("--height=640");
        return options.merge(capabilities);
    }


}
